﻿using UnityEngine;
using UnityEngine.UI;

public class ButtonScript : MonoBehaviour
{
    ViewModule view = ModulesManager.View;
    public static int StepCount = 0;

    public void button_onClick()
    {
        if (view.gameState != GameState.Shuffle)
        {
            StepCount++;
            ModulesManager.UIModule.OnNewStep();
            view.Move(gameObject);
        }
    }
}
