﻿using System;

public class WWWTextureException : Exception
{
    public WWWTextureException()
    {
        ModulesManager.UIModule.OnLoadingComplete();
        ModulesManager.UIModule.OnImageLinkThrowException();
    }

    public override string Message
    {
        get
        {
            return "Неверная ссылка на изображение!";
        }
    }

    public override string ToString()
    {
        return Message;
    }
}
