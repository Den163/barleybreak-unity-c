﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

    /// <summary>
    /// Интерфейс, который реализуют MVC модули
    /// </summary>
    public interface IManager
    {
        void Startup();
    }
