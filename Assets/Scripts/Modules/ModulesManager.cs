﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ModulesManager : MonoBehaviour
{
    // Переменные MVC модулей
    public static ViewModule View;
    public static UIManager UIModule;

    private IManager[] mvcs;

    [SerializeField] GameObject shuffleButton;


    // Use this for initialization
    void Awake()
    {
        shuffleButton.GetComponent<Button>().enabled = false;
        View = GetComponent<ViewModule>();
        UIModule = GetComponent<UIManager>();

        // Инициализация модулей
        mvcs = new IManager[] { View, UIModule };
        for (int i = 0; i < mvcs.Length; i++)
        {
            mvcs[i].Startup();
            Debug.Log(string.Format("Загрузка: {0}/{1}...", (i + 1), mvcs.Length));
        }

        Debug.Log("Все модули запущены");

        shuffleButton.GetComponent<Button>().enabled = true;
    }
}
