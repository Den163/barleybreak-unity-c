﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// Класс, содержащий методы для работы с объектами пользовательского интерфейса
/// </summary>
public class UIManager : MonoBehaviour, IManager
{

    [SerializeField]
    Text difficlutyNumLabel;           // Метка сложности
    [SerializeField]
    Slider difficultySlider;           // Ползунок сложности
    [SerializeField]
    Dropdown gameModesDropDown;        // Выпадающий список режимов игры
    [SerializeField]
    InputField imageLinkInputField;    // Поле для ввода ссылки на изображение
    [SerializeField]
    Image startMenu;                   // Начальное меню
    [SerializeField]
    Image imageLinkErrorWindow;        // Окно ошибки о ссылке на неверное изображение
    [SerializeField]
    Image loadingWindow;               // Окно загрузки
    [SerializeField]
    Button tipButton;                  // Кнопка подсказок
    [SerializeField]
    Image shuffleWindow;               // Окно перемешивания кнопок
    [SerializeField]
    Text stepCountText;                // Текст с количеством шагов
    [SerializeField]
    Image winWindow;                   // Окно победы 

    ViewModule view;

    bool isTipsOn = false;             // Включены ли подсказки

    public void Startup()
    {
        InitializeVariables();

#if UNITY_ANDROID
        imageLinkInputField.transform.GetChild(0).GetComponent<Text>().text = 
            "Вставьте ссылку на изображение в поле ввода";
#endif

        Debug.Log(@"Модуль ""Менеджер интерфейсов"" инициализирован");
    }

    private void InitializeVariables()
    {
        view = ModulesManager.View;
        OnDifficultyChange();
        OnGameModeChange();
    }

    public void OnDifficultyChange()
    {
        view.FACTOR = (int)difficultySlider.value;
        difficlutyNumLabel.text = string.Format("{0}x{0}", view.FACTOR);
    }

    public void OnGameModeChange()
    {
        switch (gameModesDropDown.value)
        {
            case 0:
                view.gameMode = GameMode.Image;
                tipButton.gameObject.SetActive(true);
                imageLinkInputField.gameObject.SetActive(true);
                if (ViewModule.ImageLink != null)
                    imageLinkInputField.text = ViewModule.ImageLink;
                break;
            case 1:
                view.gameMode = GameMode.Nums;
                tipButton.gameObject.SetActive(false);
                imageLinkInputField.gameObject.SetActive(false);
                break;
        }
    }

    public void OnStartButtonClick()
    {
        ButtonScript.StepCount = 0;
        ViewModule.ImageLink = imageLinkInputField.text;
        view.FullInitialize();
        startMenu.gameObject.SetActive(false);
    }

    public void OnLoadingBegin()
    {
        loadingWindow.gameObject.SetActive(true);
    }

    public void OnLoadingComplete()
    {
        loadingWindow.gameObject.SetActive(false);
    }

    public void OnImageButtonClick()
    {
        view.Move(gameObject);
    }

    public void OnShuffleButtonClick()
    {
        view.Shuffle();
        ButtonScript.StepCount = 0;
    }

    public void OnImageLinkThrowException()
    {
        imageLinkErrorWindow.gameObject.SetActive(true);
        startMenu.gameObject.SetActive(true);
    }

    public void OnImageLinkErrorClick()
    {
        imageLinkErrorWindow.gameObject.SetActive(false);
        SceneManager.LoadScene(0);
    }

    public void OnHomeButtonClick()
    {
        SceneManager.LoadScene(0);
    }

    public void OnTipButtonClick()
    {
        if (!isTipsOn)
            view.SetTipsOn();
        else
            view.SetTipsOff();

        isTipsOn = !isTipsOn;
    }

    public void OnShuffleBegin()
    {
        shuffleWindow.gameObject.SetActive(true);
    }

    public void OnShuffleComplete()
    {
        shuffleWindow.gameObject.SetActive(false);
    }

    public void OnExitButtonClick()
    {
        Application.Quit();
    }

    public void OnNewStep()
    {
        stepCountText.text = ButtonScript.StepCount.ToString();
    }

    public void OnWin()
    {
        stepCountText.text = "";
        winWindow.gameObject.SetActive(true);
        winWindow.transform.GetChild(0).GetComponent<Text>().text = 
        "Поздравляем!\nВы решили задачу за " + ButtonScript.StepCount + " ходов";
    }


}
