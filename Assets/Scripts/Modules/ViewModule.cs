﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public enum GameState
{
    Idle,
    Shuffle,
    Moving,
    Win
}

public enum GameMode
{
    Image,
    Nums
}

public class ViewModule : MonoBehaviour, IManager
{
    public GameState gameState = GameState.Idle;
    public GameMode gameMode = GameMode.Image;

    [SerializeField]
    RawImage image;                        // Переменная области изображения
    [SerializeField]
    Camera mainCamera;                     // Ссылка на главную камеру
    [SerializeField]
    GameObject buttonPrefab;               // Ссылка на префаб (шаблон) кнопки (кусочка изображения)
    [SerializeField]
    Color invisibleColor = Color.white;    // Цвет невидимой кнопки (alpha channel = 0)

    GameObject[,] imageButtons;             // 2d массив для представления кнопок (как отдельных кусочков изображения)
    GameObject[,] originImageButtons;       // Изначальное положение кнопок с изображениями

    UIManager uiManager;

    Vector2 imageSize;                      // Размер области изображения 
    Vector2 buttonSize;                     // Размер кнопки

    Vector2 freeCell;                       // Индекс строки и столбца свободной ячейки соответственно (x = i, y = j)

    Texture2D originTexture;                // Скачиваемое изображение

    float buttonWidth;                      // Ширина кнопки
    float buttonHeight;                     // Высота кнопки

    public static string ImageLink;         // Ссылка на изображение в www

    public int FACTOR = 5;                  // Коэффицент деления изображения

    // Запуск представления
    public void Startup()
    {
#if UNITY_EDITOR
        Debug.Log(@"Модуль ""Представление"" инициализирован");
#endif
    }

    public void FullInitialize()
    {
        InitializeVariables();
        // Устанавливаем область для изображения в центр экрана
        image.transform.position = new Vector3(mainCamera.pixelWidth / 2,
                                               mainCamera.pixelHeight / 2, 0);

        // Запускаем сопрограмму, которая скачивает изображение из интернета и инстанцирует кнопки
        if (gameMode == GameMode.Image)
            StartCoroutine(DownloadTextureFromWeb(ImageLink, GetWWWTexture));
        else
            InstantiateButtons(); // Либо, если режим - классические пятнашки - просто заполняем игровое поле кнопками
    }

    private void InitializeVariables()
    {
        uiManager = ModulesManager.UIModule;
        imageButtons = new GameObject[FACTOR, FACTOR];

        imageSize = image.GetComponent<RectTransform>().sizeDelta;
        buttonWidth = imageSize.x / FACTOR;
        buttonHeight = imageSize.y / FACTOR;
        buttonSize = new Vector2(buttonWidth, buttonHeight);
    }

    //------------------------ МЕТОДЫ ДЛЯ ИНСТАНЦИРОВАНИЯ КНОПОК -----------------------------------

    // Заполняем область изображения кнопками
    private void InstantiateButtons()
    {
        int k = 1;
        for (int i = 0; i <= imageButtons.GetUpperBound(0); i++)
            for (int j = 0; j <= imageButtons.GetUpperBound(1); j++)
            {
                imageButtons[i, j] = InstantiateButton(i, j);
                imageButtons[i, j].transform.GetChild(0).GetComponent<Text>().text = k++.ToString();
                if (gameMode != GameMode.Nums)
                    imageButtons[i, j].transform.GetChild(0).GetComponent<Text>().color = invisibleColor;

                // Последняя кнопка остается невидимой для остальных передвижений кнопок
                if (i == imageButtons.GetUpperBound(0) && j == imageButtons.GetUpperBound(1))
                {
                    imageButtons[i, j].GetComponent<Button>().enabled = false;
                    imageButtons[i, j].transform.GetChild(0).GetComponent<Text>().color = invisibleColor;
                    imageButtons[i, j].GetComponent<RawImage>().color = invisibleColor;
                    freeCell = new Vector2(i, j);
#if UNITY_EDITOR
                    Debug.Log("Свободная ячейка: " + freeCell.x + ", " + freeCell.y);
#endif
                }
            }

        // Копируем массив для доступа к изначальному положению кнопок
        originImageButtons = (GameObject[,])imageButtons.Clone();
    }

    // Метод для инстанционирования и позиционирования отдельной кнопки
    private GameObject InstantiateButton(float i, float j)
    {
        GameObject obj = Instantiate(buttonPrefab, Vector3.zero, Quaternion.Euler(0, 0, 0)) as GameObject;
        obj.transform.SetParent(image.transform, false);

        RectTransform objRectTransform = obj.GetComponent<RectTransform>();

        // Устанавливаем размер кнопки относительно области изображения
        objRectTransform.sizeDelta = buttonSize;

        // Позиционируем кнопку относительно области изображения и позиции итерации двухмерного массива
        float transformButtonX = (imageSize.x - buttonWidth) / 2 - (buttonWidth * j);
        float transformButtonY = (imageSize.y - buttonHeight) / 2 - (buttonHeight * i);
        obj.transform.localPosition = new Vector3(-transformButtonX, transformButtonY, 0);

        // назначаем каждой кнопке соответствующую картинку
        if (gameMode == GameMode.Image)
            obj.GetComponent<RawImage>().texture = SetImageForButton((int)i, (int)j);

        return obj;
    }

    //------------------------ МЕТОДЫ ДЛЯ РЕАЛИЗАЦИИ ХОДА ПОЛЬЗОВАТЕЛЕМ -----------------------------------

    int clickediRow;                       //  Индекс строки последнего хода
    int clickedjColumn;                    //  Индекс столбца последнего хода  

    // Если есть рядом свободная ячейка, то выполняем перестановку
    public void Move(GameObject sender)
    {
        if (CheckForFreeCell(sender))
        {
            SwapCells(clickediRow, clickedjColumn, (int)freeCell.x, (int)freeCell.y);
            if (CheckForWin() && gameMode == GameMode.Image)
                imageButtons[(int)freeCell.x, (int)freeCell.y].GetComponent<RawImage>().color = Color.white;
            return;
        }

#if UNITY_EDITOR
        Debug.Log("Движение невозможно");
#endif
    }

    // Проверяем есть ли рядом (слева/справа или сверху/снизу) свободная для перестановки ячейка
    private bool CheckForFreeCell(GameObject sender)
    {
        var clickediRow = 0;
        var clickedjColumn = 0;

        FindButtonIndex(ref clickediRow, ref clickedjColumn, sender);

        // Проверяем, есть ли рядом свободная ячейка

        int xIndex = (int)Mathf.Abs(clickediRow - freeCell.x);
        int yIndex = (int)Mathf.Abs(clickedjColumn - freeCell.y);

        if ((xIndex == 1 && yIndex == 0) || (xIndex == 0 && yIndex == 1))
        {
#if UNITY_EDITOR
            Debug.Log("Рядом есть свободная ячейка c индексами i и j: " + freeCell.x + ", " + freeCell.y);
#endif
            return true;
        }
#if UNITY_EDITOR
        Debug.Log("Рядом нет свободной ячейки");
#endif
        return false;
    }

    private void FindButtonIndex(ref int clickediRow, ref int clickedjColumn, GameObject sender)
    {
        for (clickediRow = 0; clickediRow <= imageButtons.GetUpperBound(0); clickediRow++)
            for (clickedjColumn = 0; clickedjColumn <= imageButtons.GetUpperBound(1); clickedjColumn++)
                if (sender == imageButtons[clickediRow, clickedjColumn])
                {
#if UNITY_EDITOR
                    Debug.Log(clickediRow + ", " + clickedjColumn);
#endif
                    return;
                }
    }

    // Меняем местами ячейки (которую выбрал клиент и пустую)
    private void SwapCells(int CellAi, int CellAj, int CellBi, int CellBj)
    {
        GameObject objA = imageButtons[CellAi, CellAj];
        GameObject objB = imageButtons[CellBi, CellBj];
        GameObject temp = new GameObject();

#if UNITY_EDITOR
        Debug.Log("До перестановки: " + imageButtons[CellAi, CellAj].transform.GetChild(0).GetComponent<Text>().text +
                   ", " + imageButtons[CellBi, CellBj].transform.GetChild(0).GetComponent<Text>().text);
#endif

        temp.transform.position = objA.transform.position;
        objA.transform.position = objB.transform.position;
        objB.transform.position = temp.transform.position;

        imageButtons[CellAi, CellAj] = objB;
        imageButtons[CellBi, CellBj] = objA;

        // Новое значение свободной ячейки
        freeCell = new Vector2(CellAi, CellAj);

        Destroy(temp);

#if UNITY_EDITOR
        Debug.Log("После перестановки: " + imageButtons[CellAi, CellAj].transform.GetChild(0).GetComponent<Text>().text +
                   ", " + imageButtons[CellBi, CellBj].transform.GetChild(0).GetComponent<Text>().text);
#endif
    }

    private bool CheckForWin()
    {
        // Проверять только, если нет никаких специфичных состояний игры
        if (gameState == GameState.Idle)
        {
            for (int i = 0; i <= imageButtons.GetUpperBound(0); i++)
                for (int j = 0; j <= imageButtons.GetUpperBound(1); j++)
                    if (imageButtons[i, j] != originImageButtons[i, j])
                        return false;
#if UNITY_EDITOR
            Debug.Log("Вы выиграли");
#endif
            uiManager.OnWin();
            return true;
        }

        return false;
    }

    //------------------------ ПЕРЕМЕШИВАНИЕ КНОПОК ------------------------------------------------

    // Перемешивание кнопок параллельно запущенной программе (для исключения подвисания)
    public void Shuffle()
    {
        StartCoroutine(ShuffleCorroutine());
    }

    // ------------------------ МЕТОДЫ ДЛЯ РАБОТЫ С ИЗОБРАЖЕНИЕМ --------------------------------------

    // Метод принимающий скачанную текстуру из функции обратного вызова ImageProcessing
    private void GetWWWTexture(Texture2D inputTexture)
    {
        originTexture = inputTexture;

        // Выполннение здесь этого метода происходит, чтобы обеспечить инстанцирование кнопок только после загрузки изображения
        InstantiateButtons(); 
    }

    // Метод назначающий каждой кнопке соответствующую часть изображения
    private Texture SetImageForButton(int i, int j)
    {
        // Вычисляем ширину обрезанной текстуры относительно размера изображения и коэффицента деления
        int outputWidth = originTexture.width / FACTOR;
        int outputHeight = originTexture.height / FACTOR;
        Texture2D output = new Texture2D(outputWidth, outputHeight);

        // Копируем пиксели из области изображения в каждую [i, j] кнопку
        output.SetPixels(originTexture.GetPixels(outputWidth * j,
                                                 originTexture.height - outputHeight * (1 + i), outputWidth, outputHeight));
        output.Apply();

        return output;
    }

    // ------------------------------- МЕТОДЫ ДЛЯ ВКЛ/ВЫКЛ ПОДСКАЗОК ------------------------------------

    public void SetTipsOn()
    {
        for (int i = 0; i <= imageButtons.GetUpperBound(0); i++)
            for (int j = 0; j <= imageButtons.GetUpperBound(1); j++)
                imageButtons[i, j].transform.GetChild(0).GetComponent<Text>().color = Color.red;
    }

    public void SetTipsOff()
    {
        for (int i = 0; i <= imageButtons.GetUpperBound(0); i++)
            for (int j = 0; j <= imageButtons.GetUpperBound(1); j++)
                imageButtons[i, j].transform.GetChild(0).GetComponent<Text>().color = invisibleColor;
    }

    // ------------------------------- СОПРОГРАММЫ ------------------------------------------------------

    // Сопрограмма, симулирующая выполнение перемешивания кнопок в отдельном потоке
    IEnumerator ShuffleCorroutine()
    {
        gameState = GameState.Shuffle;
        uiManager.OnShuffleBegin();

        // Длительность перемешивания в зависимости от FACTOR 
        for (int i = 0; i < (20 * Mathf.Pow(FACTOR, 2)); i++)     
        {
            // Генерируем смещение от пустой кнопки влево/вправо или вверх/вниз
            int x = 0;
            int y = 0;
            while (true)
            {
                // Округление так, что выдается значение от -1 до 1
                x = Mathf.FloorToInt(UnityEngine.Random.Range(-1, 2));
                y = Mathf.FloorToInt(UnityEngine.Random.Range(-1, 2));

                // Отсеиваем движение по диагонали
                if ((x == 0 && y != 0) || (x != 0 && y == 0))
                    break;
            }

            int iOffset = (int)freeCell.x + x;
            int jOffset = (int)freeCell.y + y;

            // Проверяем не выходит ли сгенерированная комбинация за пределы массива
            if ((iOffset >= 0 && iOffset <= imageButtons.GetUpperBound(0)) &&
                (jOffset >= 0 && jOffset <= imageButtons.GetUpperBound(1)))
            {
                Move(imageButtons[iOffset, jOffset]);
            }

            // Возвращаем управление программе на один кадр и, далее, производим следующую итерацию
            yield return null;
        }

        gameState = GameState.Idle;
        uiManager.OnShuffleComplete();
    }

    /// <summary>
    /// Сопрограмма, которая производит запрос по ссылке на картинку и 
    /// выполняет с ней определенные клиентом действия
    /// </summary>
    /// <param name="imageLink">Ссылка на изображение</param>
    /// <param name="callBack">Метод обратного вызова, в который передается объект скачанной текстуры в качестве аргумента</param>
    IEnumerator DownloadTextureFromWeb(string imageLink, Action<Texture2D> callBack)
    {
        uiManager.OnLoadingBegin();

        WWW wwwRequest = null;

        // Запрос по ссылке
        wwwRequest = new WWW(imageLink);

        // Ожидание загрузки
        yield return wwwRequest;

        if (wwwRequest.texture.width == 8 && wwwRequest.texture.height == 8)
            throw new WWWTextureException();

#if UNITY_EDITOR
        Debug.Log("Download complete!");
#endif
        uiManager.OnLoadingComplete();

        callBack(wwwRequest.texture);
    }
}
